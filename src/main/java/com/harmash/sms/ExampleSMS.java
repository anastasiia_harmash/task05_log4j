package com.harmash.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC8791964f8c055cc1d92a46582d89c045";
    public static final String AUTH_TOKEN = "bcb467a9611601d66abba9c7e6c4e11f";

    public static void send(String msg) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380502311552"),
                        new PhoneNumber("+12562428402"), msg).create();
    }
}

